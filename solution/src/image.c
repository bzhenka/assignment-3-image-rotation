#include <image.h>


struct image createImage(const uint64_t width, const uint64_t height){
  struct image myImage = {
    .width = width,
    .height = height,
    .data = malloc(width*height*sizeof(struct pixel))
  };
  if (myImage.data == NULL) {
    return (struct image) {
      .width = 0, 
      .height = 0, 
      .data = NULL
    };
  }
  return myImage;
}

void deleteImage(struct image* myImage){
  free(myImage->data);
  myImage->width = 0;
  myImage->height = 0;
  myImage->data = NULL;
}
