#include <bmp.h>

#define HeaderBits 0x4d42
#define BitsCount 24
#define PixelSize sizeof(struct pixel)
#define HeaderSize sizeof(struct bmp_header)

#define MAX_BIT_LENGTH 4
#define RED_OFFSET 0
#define GREEN_OFFSET 1
#define BLUE_OFFSET 2
#define ZERO 0
#define ONE 1
#define PIXEL_SIZE 3

uint8_t padding(uint32_t width) {
    return (-1 * (width * sizeof(struct pixel))) % MAX_BIT_LENGTH;
}

struct bmp_header new_bmp_header(size_t width, size_t height) {
	struct bmp_header header;
	header.bfType = HeaderBits;
	header.bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel) * width * height + padding(width) * height;
	header.bfReserved = ZERO;
	header.bOffBits = sizeof(struct bmp_header);
	header.biSize = sizeof(struct bmp_header) - sizeof(uint32_t) * PIXEL_SIZE - sizeof(uint16_t);
	header.biWidth = width;
	header.biHeight = height;
	header.biPlanes = ONE;
	header.biBitCount = BitsCount;
	header.biCompression = ZERO;
	header.biSizeImage = header.bfileSize - sizeof(struct bmp_header);
	header.biXPelsPerMeter = ZERO;
	header.biYPelsPerMeter = ZERO;
	header.biClrUsed = ZERO;
	header.biClrImportant = ZERO;
	return header;
}

enum read_status from_bmp( FILE* file, struct image* img ){
    if (!file) return READ_INVALID_BITS;
    struct bmp_header header;
    if (fread(&header, HeaderSize, 1, file) != 1) return READ_INVALID_HEADER;
    if (header.bfType != HeaderBits) return READ_INVALID_SIGNATURE;
    if (header.biBitCount != BitsCount) return READ_INVALID_BITS;

    uint8_t* array = malloc(header.biSizeImage);
    fread(array, header.biSizeImage, 1, file);

    uint8_t pad = padding(header.biWidth);
    *img = createImage(header.biWidth, header.biHeight);
    for (size_t i = 0; i < header.biHeight; i++) {
        for (size_t j = 0; j < header.biWidth; j++) {
            size_t x = i * (header.biWidth * PixelSize + pad) + j * PixelSize;
            struct pixel current_pixel = {
                .red = array[x + RED_OFFSET], 
                .green = array[x + GREEN_OFFSET], 
                .blue = array[x + BLUE_OFFSET]
            };
            (img->data)[header.biWidth * i + j] = current_pixel;
        }
    }
    free(array);
    return READ_OK;
}

enum write_status to_bmp( FILE* file, struct image const* img ){
    struct bmp_header header = new_bmp_header(img->width, img->height);
    if (fwrite(&header, sizeof(header), 1, file) != 1) return WRITE_ERROR;
    if (ferror(file)) return WRITE_ERROR;

    uint8_t* array = malloc(header.biSizeImage);
    uint8_t pad = padding(header.biWidth);

    for (size_t i = 0; i < header.biHeight; i++) {
        for (size_t j = 0; j < header.biWidth; j++) {
            size_t x = i * (img->width * PixelSize + pad) + j * PixelSize;
            size_t y = i * img->width + j;
            array[x + RED_OFFSET] = (img->data)[y].red;
            array[x + GREEN_OFFSET] = (img->data)[y].green;
            array[x + BLUE_OFFSET] = (img->data)[y].blue;
        }
        for (size_t j = 0; j < pad; j++) {
            array[i * (img->width * PixelSize + pad) + img->width * PixelSize + j] = 0;
        }
    }

    if (fwrite(array, 1, header.biSizeImage, file) != header.biSizeImage) {
        free(array);
        return WRITE_ERROR;
    }

    free(array);
    return WRITE_OK;
}
