#include <rotate.h>

void rotate(struct image* myImage) {
  if (myImage->data == NULL) return;
  struct image newImage = createImage(myImage->height, myImage->width);
  for(size_t i = 0; i < myImage->height; i++) {
    for(size_t j = 0; j < myImage->width; j++){
      (newImage.data)[(myImage->width - 1 - j) * myImage->height + i] = (myImage->data)[i * myImage->width + j];
    }
  }
  deleteImage(myImage);
  *myImage = newImage;
}

void degrees(struct image* myImage, int16_t degrees) {
  for (size_t i = 0; i < degrees / 90; i++) { 
    rotate(myImage);
  }
}
