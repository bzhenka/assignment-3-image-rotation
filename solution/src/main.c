#include <bmp.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <rotate.h>

#define FULL_ROTATION_IN_DEGREE 360
#define ONE_ROTATION_IN_DEGREE 90
#define MIN_ANGLE (-270)
#define MAX_ANGLE 270

bool validate_angle(int16_t angle) {
    return ((angle + FULL_ROTATION_IN_DEGREE) % ONE_ROTATION_IN_DEGREE == 0 && angle >= MIN_ANGLE && angle <= MAX_ANGLE) ? 1 : 0;
}

int main( int argc, char** argv ) {
    if (argc != 4) {
		fprintf(stderr, "Неверное количество аргументов");
		return -1;
	}

    int16_t angle = (int16_t) strtol(argv[3], NULL, 10);
    if (!validate_angle(angle)) {
		fprintf(stderr, "Неверный угол");
		return -1;
    }
    if (angle < 0) {
        angle = FULL_ROTATION_IN_DEGREE + angle;
    }

    FILE* file = fopen(argv[1], "rb");

    struct image newImage = {0};
    if (from_bmp(file, &newImage) != 0) {
        fclose(file);
        fprintf(stderr, "Не удалось прочитать файл");
        return -1;
    }
    fclose(file);

    degrees(&newImage, angle);

    FILE *newFile = fopen(argv[2], "wb");

    if (to_bmp(newFile, &newImage) != 0) {
        fclose(newFile);
        fprintf(stderr, "Не удалось записать файл");
        return -1;
    }

    if (fclose(newFile) == EOF) {
        fprintf(stderr, "could not close file");
    }

    deleteImage(&newImage);

    return 0;
}
