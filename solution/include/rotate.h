#ifndef ROTATE
#define ROTATE

#include <image.h>

#include <inttypes.h>
#include <stdlib.h>

void rotate(struct image* myImage);

void degrees(struct image* myImage, int16_t degrees);

#endif //ROTATE
