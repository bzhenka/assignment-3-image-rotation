#ifndef IMAGE
#define IMAGE

#include <inttypes.h>
#include <stdlib.h>

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel{
  uint8_t red, green, blue; 
} __attribute__((packed));

struct image createImage(uint64_t width, uint64_t height);
void deleteImage(struct image* myImage);
#endif //IMAGE
